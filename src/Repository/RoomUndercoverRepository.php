<?php

namespace App\Repository;

use App\Entity\RoomUndercover;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RoomUndercover|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomUndercover|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomUndercover[]    findAll()
 * @method RoomUndercover[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomUndercoverRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RoomUndercover::class);
    }

    // /**
    //  * @return RoomUndercover[] Returns an array of RoomUndercover objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoomUndercover
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
