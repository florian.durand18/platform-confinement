<?php

namespace App\Repository;

use App\Entity\SynoWord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SynoWord|null find($id, $lockMode = null, $lockVersion = null)
 * @method SynoWord|null findOneBy(array $criteria, array $orderBy = null)
 * @method SynoWord[]    findAll()
 * @method SynoWord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SynoWordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SynoWord::class);
    }

    // /**
    //  * @return SynoWord[] Returns an array of SynoWord objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SynoWord
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
