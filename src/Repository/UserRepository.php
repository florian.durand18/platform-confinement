<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByRole($value): ?array
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.role = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }

    public function findByIdRoom($idRoom): ?array
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.idRoom = :val')
            ->setParameter('val', $idRoom)
            ->getQuery()
            ->getResult();
    }

    public function findByRoleAndIdRoom($role, $idRoom): ?array
    {
        return $this->createQueryBuilder('u')
            ->where('u.role = :role')
            ->setParameter('role', $role)
            ->andWhere('u.idRoom = :room')
            ->setParameter('room', $idRoom)
            ->getQuery()
            ->getResult();
    }

    public function findByIsEliminatedAnsIdRoom($eliminated, $idRoom): ?array
    {
        return $this->createQueryBuilder('u')
            ->where('u.isEliminated = :eliminated')
            ->setParameter('eliminated', $eliminated)
            ->andWhere('u.idRoom = :room')
            ->setParameter('room', $idRoom)
            ->orderBy('u.orderToPlay', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
