<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    const ROLE_CIVIL = 'CIVIL';
    const ROLE_UNDERCOVER = 'UNDERCOVER';
    const ROLE_MRWHITE = 'MRWHITE';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\ManyToOne(targetEntity=RoomUndercover::class, inversedBy="usersConnected")
     */
    private $roomUndercover;

    /**
     * @ORM\OneToMany(targetEntity=Word::class, mappedBy="userId")
     */
    private $words;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasPlayed = false;

    /**
     * @ORM\OneToMany(targetEntity=Vote::class, mappedBy="userVoter")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $vote;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $role;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEliminated;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idRoom;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDisconnected;

    /**
     * @ORM\Column(type="integer", options={"default"=0})
     */
    private $orderToPlay = 0;

    /**
     * @ORM\ManyToOne(targetEntity=Skin::class, inversedBy="users")
     */
    private $skin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ipAdress;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->words = new ArrayCollection();
        $this->vote = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        // not needed for apps that do not check user passwords
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed for apps that do not check user passwords
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getRoomUndercover(): ?RoomUndercover
    {
        return $this->roomUndercover;
    }

    public function setRoomUndercover(?RoomUndercover $roomUndercover): self
    {
        $this->roomUndercover = $roomUndercover;

        return $this;
    }

    /**
     * @return Collection|Word[]
     */
    public function getWords(): Collection
    {
        return $this->words;
    }

    public function addWord(Word $word): self
    {
        if (!$this->words->contains($word)) {
            $this->words[] = $word;
            $word->setUserId($this);
        }

        return $this;
    }

    public function removeWord(Word $word): self
    {
        if ($this->words->removeElement($word)) {
            // set the owning side to null (unless already changed)
            if ($word->getUserId() === $this) {
                $word->setUserId(null);
            }
        }

        return $this;
    }

    public function getHasPlayed(): ?bool
    {
        return $this->hasPlayed;
    }

    public function setHasPlayed(bool $hasPlayed): self
    {
        $this->hasPlayed = $hasPlayed;

        return $this;
    }

    /**
     * @return Collection|Vote[]
     */
    public function getVote(): Collection
    {
        return $this->vote;
    }

    public function addVote(Vote $vote): self
    {
        if (!$this->vote->contains($vote)) {
            $this->vote[] = $vote;
            $vote->setUserVoter($this);
        }

        return $this;
    }

    public function removeVote(Vote $vote): self
    {
        if ($this->vote->removeElement($vote)) {
            // set the owning side to null (unless already changed)
            if ($vote->getUserVoter() === $this) {
                $vote->setUserVoter(null);
            }
        }

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getIsEliminated(): ?bool
    {
        return $this->isEliminated;
    }

    public function setIsEliminated(?bool $isEliminated): self
    {
        $this->isEliminated = $isEliminated;

        return $this;
    }

    public function getIdRoom(): ?int
    {
        return $this->idRoom;
    }

    public function setIdRoom(?int $idRoom): self
    {
        $this->idRoom = $idRoom;

        return $this;
    }

    public function getIsDisconnected(): ?bool
    {
        return $this->isDisconnected;
    }

    public function setIsDisconnected(?bool $isDisconnected): self
    {
        $this->isDisconnected = $isDisconnected;

        return $this;
    }

    public function getOrderToPlay(): ?int
    {
        return $this->orderToPlay;
    }

    public function setOrderToPlay(int $orderToPlay): self
    {
        $this->orderToPlay = $orderToPlay;

        return $this;
    }

    public function getSkin(): ?Skin
    {
        return $this->skin;
    }

    public function setSkin(?Skin $skin): self
    {
        $this->skin = $skin;

        return $this;
    }

    public function getIpAdress(): ?string
    {
        return $this->ipAdress;
    }

    public function setIpAdress(?string $ipAdress): self
    {
        $this->ipAdress = $ipAdress;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
