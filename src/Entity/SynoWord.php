<?php

namespace App\Entity;

use App\Repository\SynoWordRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SynoWordRepository::class)
 */
class SynoWord
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civilWord;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $undercoverWord;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCivilWord(): ?string
    {
        return $this->civilWord;
    }

    public function setCivilWord(string $civilWord): self
    {
        $this->civilWord = $civilWord;

        return $this;
    }

    public function getUndercoverWord(): ?string
    {
        return $this->undercoverWord;
    }

    public function setUndercoverWord(string $undercoverWord): self
    {
        $this->undercoverWord = $undercoverWord;

        return $this;
    }
}
