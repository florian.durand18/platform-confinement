<?php

namespace App\Entity;

use App\Repository\VoteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VoteRepository::class)
 */
class Vote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="vote")
     */
    private $userVoter;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fromUserId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fromUserUsername;

    /**
     * @ORM\Column(type="integer")
     */
    private $toUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserVoter(): ?User
    {
        return $this->userVoter;
    }

    public function setUserVoter(?User $userVoter): self
    {
        $this->userVoter = $userVoter;

        return $this;
    }

    public function getFromUserId(): ?int
    {
        return $this->fromUserId;
    }

    public function setFromUserId(?int $fromUserId): self
    {
        $this->fromUserId = $fromUserId;

        return $this;
    }

    public function getFromUserUsername(): ?string
    {
        return $this->fromUserUsername;
    }

    public function setFromUserUsername(?string $fromUserUsername): self
    {
        $this->fromUserUsername = $fromUserUsername;

        return $this;
    }

    public function getToUser(): ?int
    {
        return $this->toUser;
    }

    public function setToUser(int $toUser): self
    {
        $this->toUser = $toUser;

        return $this;
    }
}
