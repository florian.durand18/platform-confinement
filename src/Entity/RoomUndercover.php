<?php

namespace App\Entity;

use App\Repository\RoomUndercoverRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoomUndercoverRepository::class)
 */
class RoomUndercover
{
    const STATUS_WAITING = 'WAITING';
    const STATUS_PLAYING = 'PLAYING';
    const STATUS_VOTING = 'VOTING';
    const STATUS_ENDING = 'ENDING';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="integer", options={"default"=10})
     */
    private $maxPlayer = 10;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $idSalon;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="roomUndercover")
     * @ORM\OrderBy({"orderToPlay" = "ASC"})
     */
    private $usersConnected;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status = self::STATUS_WAITING;

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    private $nbTurn = 2;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     */
    private $elimination = true;

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    private $currentTurn = 1;

    /**
     * @ORM\Column(type="boolean", options={"default"=0})
     */
    private $isMrWhite = true;

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    private $nbUndercover = 1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $civilWord;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $undercoverWord;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isLastVoteEquality;

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    private $nbTurnWord = 1;

    /**
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->usersConnected = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getMaxPlayer(): ?int
    {
        return $this->maxPlayer;
    }

    public function setMaxPlayer(int $maxPlayer): self
    {
        $this->maxPlayer = $maxPlayer;

        return $this;
    }

    public function getIdSalon(): ?string
    {
        return $this->idSalon;
    }

    public function setIdSalon(string $idSalon): self
    {
        $this->idSalon = $idSalon;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsersConnected(): Collection
    {
        return $this->usersConnected;
    }

    public function addUsersConnected(User $usersConnected): self
    {
        if (!$this->usersConnected->contains($usersConnected)) {
            $this->usersConnected[] = $usersConnected;
            $usersConnected->setRoomUndercover($this);
        }

        return $this;
    }

    public function removeUsersConnected(User $usersConnected): self
    {
        if ($this->usersConnected->removeElement($usersConnected)) {
            // set the owning side to null (unless already changed)
            if ($usersConnected->getRoomUndercover() === $this) {
                $usersConnected->setRoomUndercover(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNbTurn(): ?int
    {
        return $this->nbTurn;
    }

    public function setNbTurn(int $nbTurn): self
    {
        $this->nbTurn = $nbTurn;

        return $this;
    }

    public function getElimination(): ?bool
    {
        return $this->elimination;
    }

    public function setElimination(bool $elimination): self
    {
        $this->elimination = $elimination;

        return $this;
    }

    public function getCurrentTurn(): ?int
    {
        return $this->currentTurn;
    }

    public function setCurrentTurn(int $currentTurn): self
    {
        $this->currentTurn = $currentTurn;

        return $this;
    }

    public function getIsMrWhite(): ?bool
    {
        return $this->isMrWhite;
    }

    public function setIsMrWhite(bool $isMrWhite): self
    {
        $this->isMrWhite = $isMrWhite;

        return $this;
    }

    public function getNbUndercover(): ?int
    {
        return $this->nbUndercover;
    }

    public function setNbUndercover(int $nbUndercover): self
    {
        $this->nbUndercover = $nbUndercover;

        return $this;
    }

    public function getCivilWord(): ?string
    {
        return $this->civilWord;
    }

    public function setCivilWord(?string $civilWord): self
    {
        $this->civilWord = $civilWord;

        return $this;
    }

    public function getUndercoverWord(): ?string
    {
        return $this->undercoverWord;
    }

    public function setUndercoverWord(?string $undercoverWord): self
    {
        $this->undercoverWord = $undercoverWord;

        return $this;
    }

    public function getIsLastVoteEquality(): ?bool
    {
        return $this->isLastVoteEquality;
    }

    public function setIsLastVoteEquality(?bool $isLastVoteEquality): self
    {
        $this->isLastVoteEquality = $isLastVoteEquality;

        return $this;
    }

    public function getNbTurnWord(): ?int
    {
        return $this->nbTurnWord;
    }

    public function setNbTurnWord(int $nbTurnWord): self
    {
        $this->nbTurnWord = $nbTurnWord;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
