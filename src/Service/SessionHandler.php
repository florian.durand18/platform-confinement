<?php


namespace App\Service;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionHandler
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    private function getSessionUser(Session $session)
    {
        if ($session->has('idUser')) {
            $idUser = $session->get('idUser');
            $user = $this->em->getRepository(User::class)->find($idUser);
        } else {
            return null;
        }
        return $user;
    }

}