<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index(): Response
    {
//        $user_1 = new User();
//        $user_1->setUsername('User_1');
//        $user_1->setIsDisconnected(false);
//        $user_1->setIdRoom('12345');
//        $user_1->setRole(User::ROLE_CIVIL);
//        $user_2 = new User();
//        $user_2->setUsername('User_2');
//        $user_2->setIsDisconnected(false);
//        $user_2->setIdRoom('12345');
//        $user_2->setRole(User::ROLE_CIVIL);
//        $user_3 = new User();
//        $user_3->setUsername('User_3');
//        $user_3->setIsDisconnected(false);
//        $user_3->setIdRoom('12345');
//        $user_3->setRole(User::ROLE_CIVIL);
//        $user_4 = new User();
//        $user_4->setUsername('User_4');
//        $user_4->setIsDisconnected(false);
//        $user_4->setIdRoom('12345');
//        $user_4->setRole(User::ROLE_CIVIL);
//
        $em = $this->getDoctrine()->getManager();
//
//        $em->persist($user_1);
//        $em->persist($user_2);
//        $em->persist($user_3);
//        $em->persist($user_4);
//
//        $em->flush();

        $civils = $em->getRepository(User::class)->findByRoleAndIdRoom(User::ROLE_CIVIL, '65');
        $undercovers = $em->getRepository(User::class)->findByRoleAndIdRoom(User::ROLE_UNDERCOVER, '65');
        $mrWhites = $em->getRepository(User::class)->findByRoleAndIdRoom(User::ROLE_MRWHITE, '65');

        $countUndercoversLeft = 0;
        $countCivilsLeft = 0;
        $countMrWhite = 0;

        foreach ($civils as $civil) {
            if ($civil && !$civil->getIsEliminated()) {
                $countCivilsLeft++;
            }
        }

        foreach ($undercovers as $undercover) {
            if ($undercovers && !$undercover->getIsEliminated()) {
                $countUndercoversLeft++;
            }
        }

        foreach ($mrWhites as $mrWhite) {
            if ($mrWhite && !$mrWhite->getIsEliminated()) {
                $countMrWhite++;
            }
        }

        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
            'civils' => $civils,
            'undercovers' => $undercovers,
            'countCivils' => $countCivilsLeft,
            'countUndercovers' => $countUndercoversLeft,
            'countMrWhite' => $countMrWhite,
        ]);
    }

    /**
     * @Route("/testRoute", name="testRoute")
     * @param PublisherInterface $publisher
     * @param Request $request
     * @return Response
     */
    public function testRoute(PublisherInterface $publisher, Request $request) {

    }
}
