<?php

namespace App\Controller;

use App\Entity\RoomUndercover;
use App\Entity\Skin;
use App\Entity\SynoWord;
use App\Entity\User;
use App\Entity\Vote;
use App\Entity\Word;
use Detection\MobileDetect;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="undercover_")
 */
class UndercoverController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $mobileDetect = new MobileDetect();

        if ($mobileDetect->isTablet()) {
            return $this->render('home/index.html.twig', []);
        }

        if ($mobileDetect->isMobile()) {
            return $this->render('home/index-mobile.html.twig', []);
        }

        return $this->render('home/index.html.twig', []);
    }


    /**
     * @Route("/contribute/", name="contribute")
     * @return Response
     */
    public function contribute(): Response
    {
        return $this->render('home/contribute.html.twig', []);
    }

    /**
     * @Route("/presse/", name="presse")
     * @return Response
     */
    public function presse(): Response
    {
        return $this->render('home/presse.html.twig', []);
    }

    /**
     * @Route("/rules/", name="rules")
     * @return Response
     */
    public function rules(): Response
    {
        return $this->render('home/rules.html.twig', []);
    }



    /**
     * @Route("/create/", name="create")
     * @return Response
     */
    public function create(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        if (!empty($request->cookies->get("undercovergame"))) {
            $decodedCookie = base64_decode($_COOKIE["undercovergame"]);
            $user = $em->getRepository(User::class)->findOneBy(['ipAdress' => $decodedCookie]);
        }

        if (empty($user)) {
            $user = new User();
        }

        $formQuick = $this->createFormBuilder()
            ->setAction($this->generateUrl('undercover_form_create_room'))
            ->setMethod('POST')
            ->getForm();

        $formPerso = $this->createFormBuilder()
            ->setAction($this->generateUrl('undercover_form_create_room'))
            ->setMethod('POST')
            ->getForm();

        return new Response(
            $this->render('undercover/create.html.twig', [
                'formQuick' => $formQuick->createView(),
                'formPerso' => $formPerso->createView(),
                'platformUrl' => $this->getParameter('platform_url'),
                'user' => $user,
            ])
        );
    }

    /**
     * @Route("/form/createRoom", name="form_create_room")
     * @param Request $request
     * @param Session $session
     * @return Response
     */
    public function formCreateRoom(Request $request, Session $session)
    {
        $em = $this->getDoctrine()->getManager();

        if (!empty($_COOKIE["undercovergame"])) {
            $decodedCookie = base64_decode($_COOKIE["undercovergame"]);
            $user = $em->getRepository(User::class)->findOneBy(['ipAdress' => $decodedCookie]);
        }

        if (empty($user)) {
            $user = new User();
        }

        /** @var Skin $skin */
        $skin = $em->getRepository(Skin::class)->find($request->request->get('formQuickSkinId'));

        if ($request->isMethod('POST')) {

            $user->setUsername($request->request->get('formQuickUsername'));
            $user->setSkin($skin);
            $user->setRole(User::ROLE_CIVIL);
            $user->setIsEliminated(false);
            $user->setIpAdress($request->getClientIp());
            $em->persist($user);
            $em->flush();
            $session->set('idUser', $user->getId());

            $roomUndercover = new RoomUndercover();
            $roomUndercover->setOwner($user);
            $roomUndercover->setUrl(strtoupper(uniqid()));
            $roomUndercover->setIdSalon($this->generateRandomString());
            $roomUndercover->setStatus('WAITING');
            $em->persist($roomUndercover);
            $em->flush();
            $user->setIdRoom($roomUndercover->getId());
            $em->flush();
            $session->set('idRoom', $roomUndercover->getId());

            return $this->redirectToRoute('undercover_salon', ['slug' => $roomUndercover->getUrl()]);
        }

        return $this->redirectToRoute('undercover_create');
    }

    /**
     * @Route("/form/joinRoom", name="form_join_room")
     * @param Request $request
     * @param Session $session
     * @return RedirectResponse
     */

    public function formJoinRoom(Request $request, Session $session)
    {
        $em = $this->getDoctrine()->getManager();

        if (!empty($_COOKIE["undercovergame"])) {
            $decodedCookie = base64_decode($_COOKIE["undercovergame"]);
            $user = $em->getRepository(User::class)->findOneBy(['ipAdress' => $decodedCookie]);
        }

        if (empty($user)) {
            $user = new User();
        }

        /** @var Skin $skin */
        $skin = $em->getRepository(Skin::class)->find($request->request->get('formJoinSkinId'));

        if ($request->isMethod('POST')) {

            /** @var RoomUndercover $roomUndercover */
            $roomUndercover = $em->getRepository(RoomUndercover::class)->findOneBy(['url' => $request->request->get('formJoinSlug')]);

            $user->setUsername($request->request->get('formJoinUsername'));
            $user->setRole(User::ROLE_CIVIL);
            $user->setSkin($skin);
            $user->setIdRoom($roomUndercover->getId());
            $user->setIsEliminated(false);
            $user->setIpAdress($request->getClientIp());

            $em->persist($user);
            $em->flush();

            $session->set('idUser', $user->getId());

            $session->set('idRoom', $roomUndercover->getId());

            return $this->redirectToRoute('undercover_salon', ['slug' => $roomUndercover->getUrl()]);
        }

        return $this->redirectToRoute('undercover_create');
    }

    /**
     * @Route("/{slug}", name="salon")
     * @param Request $request
     * @param Session $session
     * @param string $slug
     * @return Response
     */
    public function game(Request $request, Session $session, string $slug): Response
    {
        $em = $this->getDoctrine()->getManager();

        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->findOneBy(['url' => $slug]);

        $roomIdSession = $session->get('idRoom');
        if ($roomIdSession) {
            if ($roomIdSession !== $roomUndercover->getId()) {
                return $this->redirectToRoute('undercover_join', ['slug' => $slug]);
            }
        }

        $userId = $session->get('idUser');
        if (!empty($userId)) {
            /** @var User $user */
            $user = $em->getRepository(User::class)->find($userId);
        } else {
            return $this->redirectToRoute('undercover_join', ['slug' => $slug]);
        }

        if ($roomUndercover) {
            $session->set('idRoom', $roomUndercover->getId());
        } else {
            return new Response("Game not found", 404);
        }


        $user->setIdRoom($roomUndercover->getId());

        $em->flush();

        $cookie = new Cookie(
            'undercovergame',
            base64_encode($request->getClientIp()),
            time() + (10 * 365 * 24 * 60 * 60)
        );

        $response = $this->render('undercover/index.html.twig', [
            'mercureUrl' => $this->getParameter('mercure_url'),
            'platformUrl' => $this->getParameter('platform_url'),
            'slug' => $slug,
            'user' => $user,
            'roomUndercover' => $roomUndercover,
        ]);

        $response->headers->setCookie($cookie);

        return $response;
    }

    /**
     * @Route("/join/{slug}", name="join")
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function join(Request $request, string $slug): Response
    {
        $em = $this->getDoctrine()->getManager();

        if (!empty($request->cookies->get("undercovergame"))) {
            $decodedCookie = base64_decode($_COOKIE["undercovergame"]);
            $user = $em->getRepository(User::class)->findOneBy(['ipAdress' => $decodedCookie]);
        }

        if (empty($user)) {
            $user = new User();
        }

        $formJoin = $this->createFormBuilder()
            ->setAction($this->generateUrl('undercover_form_join_room'))
            ->getForm();

        return $this->render('undercover/join.html.twig', [
            'formJoin' => $formJoin->createView(),
            'platformUrl' => $this->getParameter('platform_url'),
            'slug' => $slug,
            'user' => $user,
        ]);
    }

    /* ------------------------------------------- */
    /*                   MERCURE                   */
    /* ------------------------------------------- */






    /**
     * @Route("/mercure/{slug}", name="mercure_undercover")
     * @param MessageBusInterface $bus
     * @param EntityManagerInterface $em
     * @param Session $session
     * @param Request $request
     * @param string $slug
     * @return Response
     */
    public function mercureMessage(MessageBusInterface $bus, EntityManagerInterface $em, Session $session, Request $request, string $slug)
    {
        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));

        if ($roomUndercover->getStatus() != "PLAYING") {
            return new Response('OK', 200);
        }

        $content = $request->getContent();
        $content = json_decode($content, true);

        $userId = $session->get('idUser');
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($userId);

        $word = new Word();

        $previousWords = $user->getWords()->toArray();

        $previousWord = array_pop($previousWords);

        $word->setWord($content['message']['message']);
        $user->addWord($word);

        $em->persist($word);
        $em->flush();

        if ($previousWord) $content['message']['previousWord'] = $previousWord->getWord();

        $content['message']['userId'] = $user->getId();
        $content['message']['username'] = $user->getUsername();
        $content['message']['currentTurn'] = $roomUndercover->getNbTurnWord();

        $platformUrl = $this->getParameter('platform_url');

        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($content, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/connectionOpened/{slug}", name="connection_opened")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param Session $session
     * @param string $slug
     * @return Response
     */
    public function mercureConnectionOpened(Request $request, MessageBusInterface $bus, Session $session, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($session->get('idUser'));

        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));

        if (
            (!$roomUndercover->getUsersConnected()->contains($user)) &&
            ($roomUndercover->getStatus() !== "WAITING")
        ) {
            return new Response('La partie a déjà commencé sans toi petit batard', 200);
        } elseif ($user->getIsDisconnected()) {

            $content['onSubscribe'] = [
                'userId' => $user->getId(),
                'gameStatus' => $roomUndercover->getStatus(),
            ];

            $platformUrl = $this->getParameter('platform_url');
            $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($content, true));
            $bus->dispatch($update);

        }

        $usersConnected = $roomUndercover->getUsersConnected();

        foreach ($usersConnected as $userConnected) {
            /** @var User $userConnected */
            if ($userConnected->getId() === $user->getId()) {
                return new Response('OK', 200);
            }
        }

        $roomUndercover->addUsersConnected($user);

        $em->flush();

        $words = [];

        foreach ($user->getWords() as $word) {
            array_push($words, $word->getWord());
        }

        $content['onSubscribe'] = [
            'userId' => $user->getId(),
            'username' => $user->getUsername(),
            'words' => $words,
            'gameStatus' => $roomUndercover->getStatus(),
            'skinPath' => $user->getSkin()->getPath(),
        ];

        $platformUrl = $this->getParameter('platform_url');
        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($content, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/connectionClosed/{slug}", name="connection_closed")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param Session $session
     * @param string $slug
     * @return Response
     */
    public function mercureConnectionClosed(Request $request, MessageBusInterface $bus, Session $session, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($session->get('idUser'));

        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));
        $usersConnected = $roomUndercover->getUsersConnected();

        if ($roomUndercover->getStatus() === RoomUndercover::STATUS_WAITING) {
            if ($usersConnected->contains($user)) {
                $roomUndercover->removeUsersConnected($user);
            }
        } else {
            $user->setIsDisconnected(true);
        }

        $em->flush();

        $content['onUnsubscribe'] = [
            'userId' => $user->getId(),
            'username' => $user->getUsername(),
            'gameStatus' => $roomUndercover->getStatus(),
        ];

        $platformUrl = $this->getParameter('platform_url');
        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($content, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/startGame/{slug}", name="start-game")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param Session $session
     * @param string $slug
     * @return Response
     */
    public function mercureStartGame(Request $request, MessageBusInterface $bus, Session $session, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($session->get('idUser'));

        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));

        $synoWords = $em->getRepository(SynoWord::class)->findAll();

        $usersConnected = $roomUndercover->getUsersConnected();

        //Random order of the game
        $randomOrder = [];
        $start = 1;
        $end = count($usersConnected);
        $indexOrder = 0;
        while (count($randomOrder) < $end) {
            $rand = rand($start, $end);
            if (!in_array($rand, $randomOrder)) {
                $randomOrder[$indexOrder] = $rand;
                $indexOrder++;
            }
        }

        $index = 0;
        foreach ($usersConnected as $userOrder) {
            $userOrder->setOrderToPlay($randomOrder[$index]);
            $index++;
        }

        $usersConnected = $roomUndercover->getUsersConnected();

        $randomIndex = rand(0, count($synoWords) - 1);

        $roomUndercover->setCivilWord($synoWords[$randomIndex]->getCivilWord());
        $roomUndercover->setUndercoverWord($synoWords[$randomIndex]->getUndercoverWord());

        // IF MRWHITE RAND UNDERCOVER + 1
        if ($roomUndercover->getIsMrWhite()) {
            $randomUndercover = [];
            $start = 0;
            $end = count($usersConnected) - 1;
            $count = $roomUndercover->getNbUndercover();

            // For undercovers
            while (count($randomUndercover) < $count) {
                $rand = rand($start, $end);
                $randomUndercover[$rand] = $rand;
            }

            $i = 0;
            foreach ($randomUndercover as $value) {
                if ($i <= $roomUndercover->getNbUndercover()) {
                    $usersConnected[$value]->setRole(User::ROLE_UNDERCOVER);
                    $i++;

                }
            }

            $mrWhiteSet = false;
            foreach ($usersConnected as $userConnected) {
//                if (!($userConnected->getRole() === User::ROLE_UNDERCOVER) && !($userConnected->getOrderToPlay() === 1)) {
                if (
                    $userConnected->getRole() === User::ROLE_CIVIL &&
                    $userConnected->getOrderToPlay() !== 1 &&
                    !$mrWhiteSet
                ) {
                    $userConnected->setRole(User::ROLE_MRWHITE);
                    $mrWhiteSet = true;
                }
            }

        } else {
            $random = [];
            $start = 0;
            $end = count($usersConnected) - 1;
            $count = $roomUndercover->getNbUndercover();
            while (count($random) < $count) {
                $rand = rand($start, $end);
                $random[$rand] = $rand;
            }
            foreach ($random as $value) {
                $usersConnected[$value]->setRole(User::ROLE_UNDERCOVER);
            }
        }

        $em->flush();

        $users = [];

        foreach ($usersConnected as $userConnect) {
            $users[] = [
                'id' => $userConnect->getId(),
                'role' => $userConnect->getRole(),
            ];
        }

        $content['onStart'] = [
            'civilWord' => $roomUndercover->getCivilWord(),
            'undercoverWord' => $roomUndercover->getUndercoverWord(),
            'users' => $users,
        ];

        $platformUrl = $this->getParameter('platform_url');
        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($content, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/counterTimedOut/{slug}", name="counter_timer_out")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param Session $session
     * @param string $slug
     * @return Response
     *
     */
    public function mercureCounterTimedOut(Request $request, MessageBusInterface $bus, Session $session, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));

        if ($roomUndercover->getStatus() == "VOTING") {
            return new Response(200, 'ok');
        }

        $content = $request->getContent();
        $content = json_decode($content, true);
        $content = $content['message'];

        if (!empty($content['userPlayed'])) {
            $userHasPlayed = $em->getRepository(User::class)->find($content['userPlayed']);
            $userHasPlayed->setHasPlayed(true);
        }

        $roomUndercover->setStatus('PLAYING');
        $em->flush();

        $nextPlayerFound = false;

        $usersConnected = $roomUndercover->getUsersConnected();

        if ($roomUndercover->getIsLastVoteEquality()) {
            foreach ($usersConnected as $UserConnected) {
                /** @var User $UserConnected */
                $UserConnected->setHasPlayed(false);
                $roomUndercover->setIsLastVoteEquality(false);
            }
        }

        // S'IL S'AGIT D'UNE GA/E AVEC ELIMINATION
        // SINON
        if ($roomUndercover->getElimination()) {

            $usersNotEliminated = $em->getRepository(User::class)->findByIsEliminatedAnsIdRoom(false, $roomUndercover->getId());

            foreach ($usersNotEliminated as $nextPlayerToplay) {
                /** @var User $UserNotEliminated */
                if (!$nextPlayerToplay->getHasPlayed() && !$nextPlayerFound) {
                    $nextPlayer = $nextPlayerToplay;
                    $nextPlayerFound = true;
                }
            }

            if ($nextPlayerFound) {

                $response['onCounterTimedOut'] = [
                    'turnOngoing' => true,
                    'userId' => $nextPlayer->getId(),
                    'username' => $nextPlayer->getUsername(),
                    'previousPlayer' => $content['userPlayed'],
                ];

            } else {

                $roomUndercover->setNbTurnWord($roomUndercover->getNbTurnWord() + 1);
                $roomUndercover->setStatus('VOTING');
                $roomUndercover->setIsLastVoteEquality(false);

                //Réinitialise tous les joueurs qui ont joué a false
                foreach ($usersConnected as $UserConnected) {
                    /** @var User $UserConnected */
                    $UserConnected->setHasPlayed(false);
                }

                $response['onCounterTimedOut'] = [
                    'turnOngoing' => false,
                    'previousPlayer' => $content['userPlayed'],
                ];

            }

        } else {

            foreach ($usersConnected as $nextPlayerToplay) {
                if (!$nextPlayerToplay->getHasPlayed() && !$nextPlayerFound) {
                    $nextPlayer = $nextPlayerToplay;
                    $nextPlayerFound = true;
                }
            }

            // Si un joueur a été trouvé
            if ($nextPlayerFound) {

                $response['onCounterTimedOut'] = [
                    'turnOngoing' => true,
                    'userId' => $nextPlayer->getId(),
                    'username' => $nextPlayer->getUsername(),
                    'previousPlayer' => $content['userPlayed'],
                ];

            } else {

                $roomUndercover->setNbTurnWord($roomUndercover->getNbTurnWord() + 1);

                //Réinitialise tous les joueurs qui ont joué a false
                foreach ($usersConnected as $UserConnected) {
                    /** @var User $UserConnected */
                    $UserConnected->setHasPlayed(false);
                }

                // Si plusieur mot par round
                if (($roomUndercover->getCurrentTurn() + 1) <= $roomUndercover->getNbTurn()) {

                    $roomUndercover->setCurrentTurn($roomUndercover->getCurrentTurn() + 1);
                    $nextPlayer = $roomUndercover->getUsersConnected()[0];

                    $response['onCounterTimedOut'] = [
                        'turnOngoing' => true,
                        'userId' => $nextPlayer->getId(),
                        'username' => $nextPlayer->getUsername(),
                        'previousPlayer' => $content['userPlayed'],
                    ];

                } else {

                    $roomUndercover->setCurrentTurn(1);
                    $roomUndercover->setStatus('VOTING');
                    $roomUndercover->setIsLastVoteEquality(false);


                    $response['onCounterTimedOut'] = [
                        'turnOngoing' => false,
                        'previousPlayer' => $content['userPlayed'],
                    ];

                }


            }
        }

        $em->flush();

        $response['onCounterTimedOut']['currentTurn'] = $roomUndercover->getCurrentTurn();

        $platformUrl = $this->getParameter('platform_url');
        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/vote/{slug}", name="vote")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param Session $session
     * @param string $slug
     * @return Response
     *
     */
    public function mercureVote(Request $request, MessageBusInterface $bus, Session $session, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($session->get('idUser'));

        if ($user->getIsEliminated()) {
            return new Response('Not Allowed to Vote', 200);
        }

//        /** @var RoomUndercover $roomUndercover */
//        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));

        $content = $request->getContent();
        $content = json_decode($content, true);
        $content = $content['message'];

//        /** @var User $userVoted */
//        $userVoted = $em->getRepository(User::class)->find($content['userToVoteOn']);

        $alreadyVote = $user->getVote();

        if (empty($alreadyVote)) {
            $user->removeVote($alreadyVote[0]);
        }

        $vote = new Vote();
        $vote->setToUser($content['userToVoteOn']);
        $vote->setFromUserId($user->getId());
        $vote->setFromUserUsername($user->getUsername());
        $user->addVote($vote);

        $em->persist($vote);
        $em->flush();

        $response['onVote'] = [
            'userId' => $user->getId(),
            'username' => $user->getUsername(),
            'voteOnUserId' => $content['userToVoteOn'],
        ];

        $platformUrl = $this->getParameter('platform_url');
        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/validateVote/{slug}", name="valide-vote")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param Session $session
     * @param string $slug
     * @return Response
     */
    public function mercureValideVote(Request $request, MessageBusInterface $bus, Session $session, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($session->get('idUser'));

        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));

        $tabUserVote = [];

        $platformUrl = $this->getParameter('platform_url');

        // If no elimination
        foreach ($roomUndercover->getUsersConnected() as $userConnected) {
            $userConnected->setHasPlayed(false);
            if ((!empty ($userConnected->getVote()[0])) && (!empty ($userConnected->getVote()[0]->getToUser()))) {
                array_push($tabUserVote, $userConnected->getVote()[0]->getToUser());
            }
        }

        $votes = array_count_values($tabUserVote);

        $maxVote = max(array_count_values($tabUserVote));
        $occurence = 0;

        foreach ($votes as $vote) {
            if ($vote === $maxVote) {
                $occurence++;
            }
        }

        if ($occurence >= 2) {

            $roomUndercover->setStatus(RoomUndercover::STATUS_PLAYING);
            $roomUndercover->setIsLastVoteEquality(true);
            $em->flush();

            $response['onValidateVote'] = [
                'isEquality' => true,
                'occurence' => $occurence,
                'votes' => json_encode($votes, true),
            ];

            $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
            $bus->dispatch($update);
            return new Response('OK', 200);

        }

        $idEliminated = array_search($maxVote, $votes);

        if ($roomUndercover->getElimination()) {

            /** @var User $userElimininated */
            $userElimininated = $em->getRepository(User::class)->find($idEliminated);
            $userElimininated->setIsEliminated(true);

            $roomUndercover->setStatus(RoomUndercover::STATUS_PLAYING);
            $em->flush();

            $civils = $em->getRepository(User::class)->findByRoleAndIdRoom(User::ROLE_CIVIL, $roomUndercover->getId());
            $undercovers = $em->getRepository(User::class)->findByRoleAndIdRoom(User::ROLE_UNDERCOVER, $roomUndercover->getId());
            $mrWhites = $em->getRepository(User::class)->findByRoleAndIdRoom(User::ROLE_MRWHITE, $roomUndercover->getId());

            $countUndercoversLeft = 0;
            $countCivilsLeft = 0;
            $countMrWhite = 0;

            foreach ($civils as $civil) {
                if ($civil && !$civil->getIsEliminated()) {
                    $countCivilsLeft++;
                }
            }

            foreach ($undercovers as $undercover) {
                if ($undercover && !$undercover->getIsEliminated()) {
                    $countUndercoversLeft++;
                }
            }

            foreach ($mrWhites as $mrWhite) {
                if ($mrWhite && !$mrWhite->getIsEliminated()) {
                    $countMrWhite++;
                }
            }

            /* ---------------------------------------------- */
            /* CONDITIONS DE VICTOIRE :                       */
            /*      - Civils :                                */
            /*          - C>0 , U=0 , M=0                     */
            /*                                                */
            /*      - Undercovers :                           */
            /*          - C=1 , U=1 , M=0                     */
            /*          - C=0 , U>0 , M=0                     */
            /*                                                */
            /*      - MrWhite :                               */
            /*          - C=1 , U=0 , M=1                     */
            /*                                                */
            /*      - MrWhite(popup) :                        */
            /*          - C=0 , U=1 , M=1                     */
            /*                                                */
            /* ---------------------------------------------- */
            $winner = false;
            if ($countCivilsLeft >= 1 && $countUndercoversLeft === 0 && $countMrWhite === 0) {
                //TODO VICTOIRE DES CIVILS
                $winner = true;
                $response['onValidateVote'] = [
                    'isEquality' => false,
                    'isElimination' => true,
                    'isWinners' => true,
                    'winnerData' => [
                        'whoHasWin' => 'CIVILS',
                    ],
                    'CountCivil' => $countCivilsLeft,
                    'CountUndercover' => $countUndercoversLeft,
                    'CountMrWhite' => $countMrWhite,
                ];
                $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
                $bus->dispatch($update);
                return new Response('OK', 200);
            }

            if (
                ($countCivilsLeft === 1 && $countUndercoversLeft === 1 && $countMrWhite === 0) ||
                ($countCivilsLeft === 0 && $countUndercoversLeft >= 1 && $countMrWhite === 0)) {
                //TODO VICTOIRE DES UNDERCOVERS
                $winner = true;
                $response['onValidateVote'] = [
                    'isEquality' => false,
                    'isElimination' => true,
                    'isWinners' => true,
                    'winnerData' => [
                        'whoHasWin' => 'UNDERCOVER',
                    ],
                    'CountCivil' => $countCivilsLeft,
                    'CountUndercover' => $countUndercoversLeft,
                    'CountMrWhite' => $countMrWhite,
                ];
                $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
                $bus->dispatch($update);
                return new Response('OK', 200);
            }
            if (
                ($countCivilsLeft === 1 && $countUndercoversLeft === 0 && $countMrWhite === 1) ||
                ($countCivilsLeft === 0 && $countUndercoversLeft === 1 && $countMrWhite === 1)
            ) {
                //TODO VICTOIRE DE MRWHITE
                $winner = true;
                $response['onValidateVote'] = [
                    'isEquality' => false,
                    'isElimination' => true,
                    'isWinners' => true,
                    'winnerData' => [
                        'whoHasWin' => 'MRWHITE',
                    ],
                    'CountCivil' => $countCivilsLeft,
                    'CountUndercover' => $countUndercoversLeft,
                    'CountMrWhite' => $countMrWhite,
                ];
                $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
                $bus->dispatch($update);
                return new Response('OK', 200);
            }

            if ($userElimininated->getRole() === User::ROLE_MRWHITE) {
                //TODO AFFICHER POPUP MRWHITE
                $response['onValidateVote'] = [
                    'isEquality' => false,
                    'isElimination' => true,
                    'isWinners' => false,
                    'eliminatedData' => [
                        'isMrWhite' => true,
                        'userId' => $userElimininated->getId(),
                        'userName' => $userElimininated->getUsername(),
                        'userRole' => $userElimininated->getRole(),
                        'userAvatar' => $userElimininated->getSkin()->getPath(),
                    ],
                    'countCivil' => $countCivilsLeft,
                    'countUndercover' => $countUndercoversLeft,
                    'countMrWhite' => $countMrWhite,
                ];
                $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
                $bus->dispatch($update);
                return new Response('OK', 200);
            }

            if (!$winner) {
                $response['onValidateVote'] = [
                    'isEquality' => false,
                    'isElimination' => true,
                    'isWinners' => false,
                    'eliminatedData' => [
                        'userId' => $userElimininated->getId(),
                        'userName' => $userElimininated->getUsername(),
                        'userRole' => $userElimininated->getRole(),
                        'userAvatar' => $userElimininated->getSkin()->getPath(),
                    ],
                    'countCivil' => $countCivilsLeft,
                    'countUndercover' => $countUndercoversLeft,
                    'countMrWhite' => $countMrWhite,
                ];
            }

        } else {

            /** @var User $userUndercover */
            $userUndercover = $em->getRepository(User::class)->findByRoleAndIdRoom(User::ROLE_UNDERCOVER, $roomUndercover->getId())[0];

            if ($userUndercover->getId() === $idEliminated) {
                $response['onValidateVote'] = [
                    'isEquality' => false,
                    'isElimination' => false,
                    'isWinners' => true,
                    'winnerData' => [
                        'whoHasWin' => 'CIVILS',
                    ]
                ];
            } else {
                $response['onValidateVote'] = [
                    'isEquality' => false,
                    'isElimination' => false,
                    'isWinners' => true,
                    'winnerData' => [
                        'whoHasWin' => 'UNDERCOVER',
                    ]
                ];
            }

            $roomUndercover->setStatus(RoomUndercover::STATUS_ENDING);

            $em->flush();
        }

        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/mrWhiteResponse/{slug}", name="mrwhite_response")
     * @param Request $request
     * @param MessageBusInterface $bus
     * @param Session $session
     * @param string $slug
     * @return Response
     */
    public function mercureMrWhiteResponse(Request $request, MessageBusInterface $bus, Session $session, string $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->find($session->get('idUser'));

        /** @var RoomUndercover $roomUndercover */
        $roomUndercover = $em->getRepository(RoomUndercover::class)->find($session->get('idRoom'));

        $content = $request->getContent();
        $content = json_decode($content, true);

        $hasFound = false;
        if ($content['message']['mrWhiteResponse'] === $roomUndercover->getCivilWord()) {
            $hasFound = true;
        }

        $response['onMrWhiteResponse'] = [
            'hasFound' => $hasFound,
        ];

        $platformUrl = $this->getParameter('platform_url');
        $update = new Update($platformUrl . "/mercure/" . $slug, json_encode($response, true));
        $bus->dispatch($update);
        return new Response('OK', 200);
    }

    /**
     * @Route("/api/skins"), name="choose_skin"
     */
    public function chooseSkin(Request $request)
    {
        $message = json_decode($request->getContent(), true);

        $type = $message['type'];

        $em = $this->getDoctrine();

        $skins = $em->getRepository(Skin::class)->findByType($type);

        $currentIndex = $message['currentIndex'];

        $data = [
            'id' => $skins[$currentIndex]->getId(),
            'name' => $skins[$currentIndex]->getName(),
            'totalSkin' => count($skins),
        ];

        $response = new Response();
        $response->setContent(json_encode([
            'data' => $data,
        ]));
        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode(200);

        return $response;
    }

    private function generateRandomString($length = 8, $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
